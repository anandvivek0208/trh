-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 02, 2021 at 04:05 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.2.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `trh_store`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` int(50) NOT NULL,
  `vid` int(50) NOT NULL,
  `category` varchar(50) NOT NULL,
  `pname` varchar(50) NOT NULL,
  `pdes` varchar(50) NOT NULL,
  `quantity` int(50) NOT NULL,
  `amount` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `vid`, `category`, `pname`, `pdes`, `quantity`, `amount`) VALUES
(1, 6, 'Vegitable', '0', '0', 100, 100);

-- --------------------------------------------------------

--
-- Table structure for table `product_category`
--

CREATE TABLE `product_category` (
  `category_id` int(100) NOT NULL,
  `vid` int(100) NOT NULL,
  `cat_name` varchar(10) NOT NULL,
  `cat_des` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_category`
--

INSERT INTO `product_category` (`category_id`, `vid`, `cat_name`, `cat_des`) VALUES
(1, 6, 'Vegitable', 'sabzi');

-- --------------------------------------------------------

--
-- Table structure for table `user_reg`
--

CREATE TABLE `user_reg` (
  `cid` int(10) NOT NULL,
  `cfname` varchar(30) NOT NULL,
  `clname` varchar(30) NOT NULL,
  `cemail` varchar(30) NOT NULL,
  `cmobile` varchar(10) NOT NULL,
  `cpassword` varchar(8) NOT NULL,
  `crpassword` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_reg`
--

INSERT INTO `user_reg` (`cid`, `cfname`, `clname`, `cemail`, `cmobile`, `cpassword`, `crpassword`) VALUES
(9, 'Vivek', 'Anand', 'anandvivek0208@gmail.com', '+917739344', '12345', '12345');

-- --------------------------------------------------------

--
-- Table structure for table `vend_reg`
--

CREATE TABLE `vend_reg` (
  `vid` int(10) NOT NULL,
  `vfname` varchar(30) NOT NULL,
  `vlname` varchar(30) NOT NULL,
  `vemail` varchar(30) NOT NULL,
  `vmobile` varchar(10) NOT NULL,
  `vpassword` varchar(8) NOT NULL,
  `vrpassword` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vend_reg`
--

INSERT INTO `vend_reg` (`vid`, `vfname`, `vlname`, `vemail`, `vmobile`, `vpassword`, `vrpassword`) VALUES
(6, 'Vivek', 'Anand', 'anandvivek0208@gmail.com', '+917739344', '12345', '12345');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `product_category`
--
ALTER TABLE `product_category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `user_reg`
--
ALTER TABLE `user_reg`
  ADD PRIMARY KEY (`cid`);

--
-- Indexes for table `vend_reg`
--
ALTER TABLE `vend_reg`
  ADD PRIMARY KEY (`vid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `product_category`
--
ALTER TABLE `product_category`
  MODIFY `category_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_reg`
--
ALTER TABLE `user_reg`
  MODIFY `cid` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `vend_reg`
--
ALTER TABLE `vend_reg`
  MODIFY `vid` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
